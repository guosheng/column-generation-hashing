# Learning Hash Functions using Column Generation

author: Guosheng Lin <guosheng.lin@adelaide.edu.au>

correspondence should be addressed to Chunhua Shen <chhshen@gmail.com>


__This is the implementation of the following paper. If you use this code in your research,
please cite our paper__

```
 @inproceedings{HashICML13a,
   author    = "Xi Li and  Guosheng Lin and  Chunhua Shen and  Anton. {van den Hengel} and  Anthony. Dick",
   title     = "Learning hash functions using column generation",
   booktitle = "International Conference on Machine Learning (ICML'13)",
   address   = "Atlanta, USA",
   year      = "2013",
 }
```

The paper can be downloaded at:

-- <http://jmlr.org/proceedings/papers/v28/li13a.pdf>; and 

-- <https://bitbucket.org/guosheng/column-generation-hashing/downloads/ICML2013a.pdf> (with the supplementary document).

The first two authors contribute equally.



## Install

The LBFGS-B solver is used our implementation. A MATLAB wrapper of LBFGS-B is included here (pre-compiled in Linux, OSX),
    which can be downloaded in: <https://github.com/pcarbo/lbfgsb-matlab>.

The file demo.m shows that how to use the code.


## Copyright

Copyright (c) Guosheng Lin, Chunhua Shen. 2013.

This code provided here is for non-commercial research purposes only.
For commercial applications, please contact Chunhua Shen <http://www.cs.adelaide.edu.au/~chhshen/>.

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.




