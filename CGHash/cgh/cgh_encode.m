


% Code author: Guosheng Lin. Contact: guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au


function feat_data_code=cgh_encode(model, feat_data, bit_num)


if isfield(model, 'wl_kernel_model')
    wl_kernel_model=model.wl_kernel_model;
    feat_data=wl_kernel_model.apply_fn(wl_kernel_model, feat_data);
end


if nargin<3
    bit_num=[];
end

if ~isempty(bit_num)
    model.hs=model.hs(1:bit_num,:);
end

feat_data_code=apply_wlearner(feat_data, model.hs, model.wl_model);

end




