


% Code author: Guosheng Lin. Contact: guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au






function train_result=cgh_train(train_info, train_data)


fprintf('\n\n-------------------------- cgh_train ------------------------------\n\n');


% for hash function learning initialization
if ~isfield(train_info, 'do_wl_train_stump')
    train_info.do_wl_train_stump=false;
end
if ~isfield(train_info, 'do_wl_train_spectral')
    train_info.do_wl_train_spectral=true;
end
if ~isfield(train_info, 'do_wl_train_random')
    train_info.do_wl_train_random=true;
end


if ~isfield(train_info, 'do_wl_train_lbfgs')
    train_info.do_wl_train_lbfgs=true;
end


if ~isfield(train_info,'tradeoff_C')
    train_info.tradeoff_C=10^6;
end
if ~isfield(train_info,'use_stagewise')
    train_info.use_stagewise=false;
end


if ~isfield(train_info, 'train_id')
	train_info.train_id='CGH';
end

train_info.cg_iter_num=train_info.bit_num;


train_info.init_work_info_fn=@init_work_info;
train_info.update_wlearner_info_fn=@update_wlearner_info;
train_info.solver_fn=@cg_solver_lbfgs;


train_result=cg_learn(train_info, train_data);

fprintf('\n\n-------------------------- cgh_train finished ------------------------------\n\n');
   
end



function work_info=update_wlearner_info(train_info, train_data, work_info, wlearner_update_info)


work_info=do_update_wlearner_info(train_info, train_data, work_info, wlearner_update_info);
new_pair_feat=calc_pair_feat(train_info, train_data, work_info, work_info.new_wlearner);

work_info.new_pair_feat=new_pair_feat;
if ~train_info.use_stagewise
    work_info.pair_feat=cat(2,	work_info.pair_feat, new_pair_feat);
end

work_info.wlearner_dimension=work_info.wlearner_dimension+size(new_pair_feat,2);

obj_value=NaN;
if ~isempty(work_info.obj_value_iters)
    obj_value=work_info.obj_value_iters(end);
end

fprintf('\n---train_id:%s, solver_info: C:%.2e, stage-wise:%d, obj_value:%.5f, cg_iter:%d(%d)\n', ...
    train_info.train_id, train_info.tradeoff_C, train_info.use_stagewise, ...
    obj_value, wlearner_update_info.wl_iter_cur, train_info.cg_iter_num);


end





function work_info=do_update_wlearner_info(train_info, train_data, work_info, wlearner_update_info)



if work_info.solver_init_iter
	pair_num=work_info.pair_num;
    pair_idxes=(1:pair_num)';
    pair_weights=ones(pair_num,1);
    
    wlearner_update_info.pair_idxes=pair_idxes;
    wlearner_update_info.pair_weights=pair_weights;
end


[new_wlearner work_info]=find_wlearner_hash(train_info, train_data, work_info, ...
    wlearner_update_info.pair_weights);


if isempty(new_wlearner)
    disp('============== WARNING: new_wlearner is empty!!!! =====================');
    assert(~isempty(work_info.wlearners));
    new_wlearner=work_info.wlearners(end,:);
end


work_info.new_wlearner=new_wlearner;
work_info.wlearners=cat(1, work_info.wlearners, new_wlearner);




end









function [work_info train_info]=init_work_info(train_info, train_data)

relation_map_triplet=train_data.triplets;

assert(isa(relation_map_triplet, 'uint32'));
assert(size(relation_map_triplet, 2)==3);

work_info.wl_model=[];
work_info.wlearners=[];
work_info.pair_feat=[];
work_info.pair_num=size(relation_map_triplet, 1);
work_info.wlearner_dimension=0;


work_info.relation_map_triplet=relation_map_triplet;

work_info.transfer_cache_duplet=gen_transfer_cache_duplet(train_info, work_info);
work_info=gen_wlearner_cache(train_info, train_data, work_info);


end







function [wlearner work_info]=find_wlearner_hash(train_info, train_data, work_info, pair_weights)


transfer_cache_duplet=work_info.transfer_cache_duplet;

pair_weights_duplet=transfer_triplet_weights(pair_weights, transfer_cache_duplet);

[wlearner work_info]=gen_wlearner(train_info, train_data, work_info, ...
	 pair_weights_duplet, transfer_cache_duplet);

end



function pair_feat=calc_pair_feat(train_info, train_data, work_info, wlearners)


    assert(size(wlearners, 1)==1);
    hfeat=apply_wlearner(train_data.feat_data, wlearners, work_info.wl_model);

    relation_map=work_info.relation_map_triplet;

    e_hfeat=hfeat(relation_map(:,1),:);
    en_hfeat=hfeat(relation_map(:,2),:);
    ep_hfeat=hfeat(relation_map(:,3),:);
        
    pair_feat=calc_hfeat_dist_bits(e_hfeat,en_hfeat)-calc_hfeat_dist_bits(e_hfeat,ep_hfeat);

end


function dist_bits=calc_hfeat_dist_bits(d1, d2)

	dist_bits=d1~=d2;
	dist_bits=double(dist_bits);

end






