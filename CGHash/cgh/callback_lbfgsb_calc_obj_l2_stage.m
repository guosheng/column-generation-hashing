function [obj_value dual_sol]=callback_lbfgsb_calc_obj_l2_stage(w, aux_data)

C_d_m=aux_data{1};
feat_data=aux_data{2};
m_rescale_vs=aux_data{3};
w_delta_h=aux_data{4};
sum_other_w=aux_data{5};

cur_mu_noexp=max(m_rescale_vs-(w_delta_h+feat_data*w),0);
loss_sum=sum(cur_mu_noexp.^2);


dual_sol=cur_mu_noexp;
obj_value=sum_other_w+w+C_d_m*loss_sum;


end