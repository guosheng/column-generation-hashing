
function [feat_data wl_model]=gen_kernel_feat(train_info, feat_data)

      
    sigma=train_info.sigma;
    support_vectors=train_info.support_vectors;     
           
    assert(~isempty(support_vectors));
        
    [n,d] = size(feat_data);
    feat_data_kernel = sqdist(feat_data',support_vectors');
   

    feat_data_kernel = exp(-feat_data_kernel/(2*sigma));
    feat_data_mean = mean(feat_data_kernel);
    feat_data_kernel = feat_data_kernel-repmat(feat_data_mean,n,1);
        
    wl_model=[];
    wl_model.support_vectors=support_vectors;
    wl_model.sigma=sigma;
    wl_model.feat_data_mean=feat_data_mean;
    wl_model.apply_fn=@apply_kernel_feat;
    

    feat_data=feat_data_kernel;
        
end






