


function transfer_cache_duplet=gen_transfer_cache_duplet(train_info, work_info)


relation_map_triplet=work_info.relation_map_triplet;
r1=relation_map_triplet(:, 1);
e_idxes=unique(r1);
e_num=e_idxes(end);

assert(e_num<2^32);


fprintf('generate cache, processing examples (%d):\n', e_num);



	triple_r_inds_duplets=cell(0);
	relation_map_duplet=cell(e_num, 1);
	relevant_sel_duplet=cell(e_num, 1);

    for e_idx_idx=1:length(e_idxes)
    	
    		e_idx=e_idxes(e_idx_idx);
        
            if mod(e_idx, 50)==0
                fprintf('%d.', e_idx);
            end
            
            if mod(e_idx, 500)==0
                fprintf('\n');
            end


            one_r_inds=find(r1==e_idx);
            one_r_inds=uint32(one_r_inds);
            if isempty(one_r_inds)
            	continue;
            end
						                    
            
            one_relation_map=relation_map_triplet(one_r_inds,:);
            pos_r_e_idxes=one_relation_map(:,3);
            neg_r_e_idxes=one_relation_map(:,2);

            pos_r_e_idxes_duplet=unique(pos_r_e_idxes);
            neg_r_e_idxes_duplet=unique(neg_r_e_idxes);

            
            
            pos_r_num=length(pos_r_e_idxes_duplet);
            neg_r_num=length(neg_r_e_idxes_duplet);
            one_r_num=pos_r_num+neg_r_num;
            
            one_r_duplet=cat(1, pos_r_e_idxes_duplet, neg_r_e_idxes_duplet);
            one_r_duplet=cat(2, repmat(e_idx, one_r_num, 1), one_r_duplet);

            one_r_relevant_sel=false(one_r_num, 1);
            one_r_relevant_sel(1:pos_r_num)=true;


            relation_map_duplet{e_idx}=one_r_duplet;
            relevant_sel_duplet{e_idx}=one_r_relevant_sel;
                 
            one_triple_r_inds_duplets=cell(one_r_num, 1);
            for nn_idx=1:one_r_num

                nn_e_idx=one_r_duplet(nn_idx,2);
                                
                if one_r_relevant_sel(nn_idx)
                    one_triple_r_inds_duplets{nn_idx}=one_r_inds(pos_r_e_idxes==nn_e_idx);
                else
                    one_triple_r_inds_duplets{nn_idx}=one_r_inds(neg_r_e_idxes==nn_e_idx);
                end
            end
            
            triple_r_inds_duplets=cat(1, triple_r_inds_duplets, one_triple_r_inds_duplets);
    end


    fprintf('\n');

    relation_map_duplet=cell2mat(relation_map_duplet);
    relevant_sel_duplet=cell2mat(relevant_sel_duplet);

    assert(isa(relation_map_duplet, 'uint32'));
    
    transfer_cache_duplet=[];               
    transfer_cache_duplet.relation_map_duplet=relation_map_duplet;
    transfer_cache_duplet.relevant_sel_duplet=relevant_sel_duplet;

    transfer_cache_duplet.triple_r_inds_duplets=triple_r_inds_duplets;

         
end


