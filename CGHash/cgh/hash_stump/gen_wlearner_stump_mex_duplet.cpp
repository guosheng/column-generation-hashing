

// Code author: Guosheng Lin. Contact: guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au


#include <cmath>
#include "mex.h"
#include "matrix.h"
#include "blas.h"
#include <time.h>
#include <limits>
#include <cstdlib>
#include <vector>
#include <iostream>
#include "my_matlib.h"

using namespace std;



//===============================================================================================================



void mexFunction( int nlhs, mxArray *plhs[], 
		  int nrhs, const mxArray*prhs[] ){ 
           
    
     
     const mxArray* relation_weights=prhs[1];
     const double* relation_weights_ptr=mxGetPr(relation_weights);
     
     const mxArray* cache_info=prhs[0];
     const mxArray* wlearner_cache=mxGetField(cache_info, 0, "wlearner_cache");
     const mxArray* thresholds_dims=mxGetField(wlearner_cache,  0, "thresholds_dims");
     const mxArray* valid_dim_inds=mxGetField(wlearner_cache,  0, "valid_dim_inds");

     const mxArray* feat_data=prhs[5];
          
     const mxArray* relation_map=prhs[3];
          
     const mxArray* relevant_sel=prhs[4];
     bool *relevant_sel_ptr=(bool *)mxGetData(relevant_sel);
         
          
     double *feat_data_ptr=mxGetPr(feat_data);
     size_t feat_num=mxGetM(feat_data);
     
     unsigned int *relation_map_ptr=(unsigned int*)mxGetData(relation_map);
     
     
     const mxArray* valid_r_inds=prhs[2];
     int valid_r_num=mxGetM(valid_r_inds);
     const double* valid_r_inds_ptr=mxGetPr(valid_r_inds);
     
     
     int dim_num=mxGetNumberOfElements(thresholds_dims);
     
     const double * valid_dim_inds_ptr=mxGetPr(valid_dim_inds);
     int valid_dim_num=mxGetNumberOfElements(valid_dim_inds);
     int disp_step=ceil(valid_dim_num/20.0);
     
          
    
     size_t r_num=mxGetM(relation_weights);
     
         
     double  *best_score_vdims_ptr=new double[valid_dim_num];
     size_t * best_thresh_idx_vdims_ptr=new size_t[valid_dim_num];
     
          
     
     #pragma omp parallel for
     for (size_t d_idx_idx=0; d_idx_idx<valid_dim_num; ++d_idx_idx){
         
         double  best_score_one_dim(-DBL_MAX);
         size_t  best_thresh_idx_one_dim(0);
              
         size_t d_idx=valid_dim_inds_ptr[d_idx_idx]-1;
         
             mxArray* threshs=mxGetCell(thresholds_dims, d_idx);
             double * thresh_ptr=mxGetPr(threshs);
             
             int thresh_num=mxGetNumberOfElements(threshs);
             double * one_col_feat_ptr=feat_data_ptr+d_idx*feat_num;
             
             unsigned int e_idx(0);
             unsigned int re_idx(0);
             bool h_e(0);
             bool rh_e(0);
             bool one_relevant_sel(0);
             
             
             for (size_t t_idx(0); t_idx<thresh_num; ++t_idx){
                
                 double one_thresh=thresh_ptr[t_idx];
                 double one_score(0);
                                  
                 
                 for (size_t r_idx_idx(0); r_idx_idx<valid_r_num; ++r_idx_idx){
                     
                    size_t r_idx=valid_r_inds_ptr[r_idx_idx]-1;

                    e_idx=relation_map_ptr[r_idx]-1;
                    re_idx=relation_map_ptr[r_idx+r_num]-1;
                    one_relevant_sel=relevant_sel_ptr[r_idx];

                     h_e=one_col_feat_ptr[e_idx]>one_thresh;
                     rh_e=one_col_feat_ptr[re_idx]>one_thresh;


                     if (one_relevant_sel){
                        one_score-=(h_e!=rh_e)*relation_weights_ptr[r_idx];
                     }else{
                        one_score+=(h_e!=rh_e)*relation_weights_ptr[r_idx];
                     }

                 }

                
                 if (one_score>best_score_one_dim){
                     best_score_one_dim=one_score;
                     best_thresh_idx_one_dim=t_idx;
                 }
             }
        
         
         
         best_score_vdims_ptr[d_idx_idx]=best_score_one_dim;
         best_thresh_idx_vdims_ptr[d_idx_idx]=best_thresh_idx_one_dim;
         
     }
  
double  best_score(-DBL_MAX);
size_t  best_thresh_idx(0);
size_t  best_dim_idx(0);
for (size_t d_idx_idx(0); d_idx_idx<valid_dim_num; ++d_idx_idx){
   if (best_score_vdims_ptr[d_idx_idx]>best_score){
       best_score=best_score_vdims_ptr[d_idx_idx];
       best_thresh_idx=best_thresh_idx_vdims_ptr[d_idx_idx];
       best_dim_idx=valid_dim_inds_ptr[d_idx_idx]-1;
   }
}


mxArray* best_dim_threshs=mxGetCell(thresholds_dims, best_dim_idx);
double best_thresh=mxGetPr(best_dim_threshs)[best_thresh_idx];


mxArray* wlearner=mxCreateDoubleMatrix(1, dim_num+1, mxREAL);
double * wlearner_ptr=mxGetPr(wlearner);
wlearner_ptr[best_dim_idx]=1;
wlearner_ptr[dim_num]=-best_thresh;
        
mxArray* wlearner_score=mxCreateDoubleScalar(best_score);

plhs[0]=wlearner;             
plhs[1]=wlearner_score;
             
delete [] best_score_vdims_ptr;
delete [] best_thresh_idx_vdims_ptr;
          
}





