function grad=callback_lbfgsb_calc_grad_l2_stage(w, aux_data)


C_d_m=aux_data{1};
feat_data=aux_data{2};
m_rescale_vs=aux_data{3};
w_delta_h=aux_data{4};

cur_mu_noexp=max(m_rescale_vs-(w_delta_h+feat_data*w),0);
grad=1-2*C_d_m*(cur_mu_noexp'*feat_data);


end

