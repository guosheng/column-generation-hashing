



% Code author: Guosheng Lin. Contact: guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au




function [train_result train_result_ext]=cg_learn(train_info, train_data)

   
[work_info train_info]=train_info.init_work_info_fn(train_info, train_data);
[train_result work_info]=do_train(train_info, train_data, work_info);

train_result_ext=train_result;
train_result_ext.work_info=work_info;
    

end




%========================================================================================================================================================


function [train_result work_info]=do_train(train_info, train_data, work_info)


cg_iter_num=train_info.cg_iter_num;

work_info.wlearners=[];
work_info.wl_model=[];
work_info.obj_value_iters=[];
work_info.wl_time_iters=[];
work_info.opt_time_iters=[];


work_info.method_time_tic=tic;

wlearner_update_info=[];
work_info.solver_init_iter=true;


wl_iter_cur=0;



while true

%-----------------------------check converge------------------------------

    if wl_iter_cur>=cg_iter_num 
        break;
    end
                
   
%-----------------------------check converge end ------------------------------


    wl_iter_cur=wl_iter_cur+1;
    wlearner_update_info.wl_iter_cur=wl_iter_cur;        
    
    one_wl_time_tic=tic;
    work_info=train_info.update_wlearner_info_fn(train_info, train_data, work_info, wlearner_update_info);
    one_wl_time=toc(one_wl_time_tic);

    
    one_opt_time_tic=tic;
    train_result_sub=train_info.solver_fn(train_info, work_info);
    one_opt_time=toc(one_opt_time_tic);

               
    work_info=train_result_sub.work_info;
    
    if isnan(train_result_sub.obj_value)
%         keyboard
        error('objective is NAN');
    end
   
       
    
    wlearner_update_info.pair_weights=train_result_sub.wlearner_pair_weight;
    wlearner_update_info.pair_idxes=train_result_sub.wlearner_pair_idxes;
    wlearner_update_info.w=train_result_sub.w;
    
    work_info.w=train_result_sub.w;              
         
    work_info.obj_value_iters(wl_iter_cur, 1)=train_result_sub.obj_value;
    work_info.wl_time_iters(wl_iter_cur, 1)=one_wl_time;
    work_info.opt_time_iters(wl_iter_cur, 1)=one_opt_time;
    
    work_info.wl_iter_cur=wl_iter_cur;
       
        
    work_info.solver_init_iter=false;
	    
end


train_result=gen_train_result(train_info, work_info);


end





function train_result=gen_train_result(train_info, work_info)


train_result=[];
train_result.train_id=train_info.train_id;
train_result.tradeoff_C=train_info.tradeoff_C;
train_result.cg_iter_num=work_info.wl_iter_cur;

train_result.obj_value_iters=work_info.obj_value_iters;
train_result.wl_time_iters=work_info.wl_time_iters;
train_result.opt_time_iters=work_info.opt_time_iters;

train_result.method_time=toc(work_info.method_time_tic);


model=[];
model.train_id=train_info.train_id;
model.w=work_info.w;
model.hs=work_info.wlearners;
model.wl_model=work_info.wl_model;
train_result.model=model;


end



